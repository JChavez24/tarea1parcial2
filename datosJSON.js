var DatosEstudiantes= [
  {
      cedula: "1317513123",
      nombre: "Arturo Borja",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"Arturo@correo.com",
      curso: "Cuarto",
      paralelo: "B"    
  },
  {
      cedula: "1307658254",
      nombre: "Jose Yepez",
      direccion:"Los esteros",
      telefono:"0999999999",
      correo:"jose@correo.com",
      curso: "Quinto",
      paralelo: "B"     
  },
  {
      cedula: "1317513123",
      nombre: "Cintia Sexto",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"cintia@correo.com",
      curso: "Sexto",
      paralelo: "A"
  },
  {
      cedula: "1317513123",
      nombre: "Isidro Romero",
      direccion:"Los esteros",
      telefono:"0999999999",
      correo:"isidro@correo.com",
      curso: "Primero",
      paralelo: "B"
  },
  {
      cedula: "1317513123",
      nombre: "Joselyn Santos",
      direccion:"Miraflores",
      telefono:"0999999999",
      correo:"joselyn@correo.com",
      curso: "Cuarto",
      paralelo: "A"
      
  },
  {
      cedula: "1307654845",
      nombre: "Edison Almeida",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"edison@correo.com",
      curso: "Cuarto",
      paralelo: "B"
      
  },
  {
      cedula: "1317513123",
      nombre: "Hector Lavoe",
      direccion:"La Aurora",
      telefono:"0999999999",
      correo:"hector@correo.com",
      curso: "Cuarto",
      paralelo: "B"
      
  },
  {
      cedula: "1307645858",
      nombre: "Paola Ramirez",
      direccion:"Las cumbres",
      telefono:"0999999999",
      correo:"paola@correo.com",
      curso: "Quinto",
      paralelo: "B"
      
  },
  {
      cedula: "1317513123",
      nombre: "Jose Chavez",
      direccion:"Las cumbres",
      telefono:"0999999999",
      correo:"josech@correo.com",
      curso: "Cuarto",
      paralelo: "B"
      
  },
  {
      cedula: "1307652824",
      nombre: "Ernesto Villa",
      direccion:"Calle 13",
      telefono:"0999999999",
      correo:"ernesto@correo.com",
      curso: "Sexto",
      paralelo: "B"
      
  }
];


function cargarEstudiantes() {
  var estuSelect = document.getElementById('estu');
  var limite = DatosEstudiantes.length;
  for (var i = 0; i < limite; i++) {
      var nombreC= DatosEstudiantes[i].nombre;
      estuSelect.options[i+1]= new Option(nombreC, nombreC)
  }

}

function CargarDatos(){
  var estuSelect = document.getElementById('estu').value;
  var limite = DatosEstudiantes.length;
  for (var i = 0; i < limite; i++) {
      var nombreC= DatosEstudiantes[i].nombre;
      if(estuSelect==nombreC){
          document.getElementById("info").innerHTML = `
          <h4>Cedula</h4>
          ${DatosEstudiantes[i].cedula}
          <h4>Nombre</h4>
          ${DatosEstudiantes[i].nombre}
          <h4>Direccion</h4>
          ${DatosEstudiantes[i].direccion}
          <h4>Telefono</h4>
          ${DatosEstudiantes[i].telefono}
          <h4>Correo</h4>
          ${DatosEstudiantes[i].correo}
          <h4>Curso</h4>
          ${DatosEstudiantes[i].curso}
          <h4>Paralelo</h4>
          ${DatosEstudiantes[i].paralelo}
          `;
      }

  }
}
